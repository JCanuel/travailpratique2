package main;

import bd.ControleConnexion;
import modele.GestionArtistes;

public class main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		ControleConnexion connec = new ControleConnexion();
		connec.connecter();
		GestionArtistes artistes = new GestionArtistes();
		artistes.getListeArtistes();
		connec.fermerSession();
	}

}
