package bd;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import javax.swing.JOptionPane;

public class ControleConnexion {
	private static Connection laConnexion;
	private static String url = "jdbc:mysql://localhost/canuel_hamel_albums";
	
	/**
	 * �tablit la connexion � la BDD si elle n'existe pas.Attention, la
	 * connexion ne doit pas �tre ferm�e
	 *
	 */
	public static void connecter() {
		try {
			
			if (laConnexion == null || laConnexion.isClosed()) {

				Class.forName("org.gjt.mm.mysql.Driver");
				laConnexion=DriverManager.getConnection(url,"root",""); 
				JOptionPane.showMessageDialog(null, "Connect\u00E9 � la BD", "ALERTE", JOptionPane.INFORMATION_MESSAGE);
			}
		} catch (ClassNotFoundException e) {
			System.out.println(e);
		} catch (SQLException sqle) {
			System.out.println(sqle);

		}
	}

	public static void fermerSession() {
		try {
			if (laConnexion != null && !laConnexion.isClosed()) {
				if (laConnexion!=null)
					laConnexion.close();
			}

		} catch (SQLException sqle) {
			System.out.println(sqle);
		}
	}

	public static Connection getLaConnexion() {
		return laConnexion;
	}

}
