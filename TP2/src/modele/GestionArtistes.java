package modele;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import javax.swing.JOptionPane;

import bd.ControleConnexion;

public class GestionArtistes {
	private Connection connection = ControleConnexion.getLaConnexion();
	/**
	 * Structure de donn�es pour chercher la liste des employ�s de la BD
	 */
	private ArrayList<Artiste> listeArtistes;

	public GestionArtistes() {
		listeArtistes = obtenirListArtistes();
	}
	public ArrayList<Artiste> getListeArtistes() {
		 return listeArtistes ;
		 } 

	private ArrayList<Artiste> obtenirListArtistes() {
		ArrayList<Artiste> liste = new ArrayList<Artiste>();
		Statement statement = null;
		ResultSet jeuResultat = null;
		String requete = "SELECT * FROM Artiste ORDER BY artiste_id";

		try {
			statement = connection.createStatement();
			jeuResultat = statement.executeQuery(requete);
			while (jeuResultat.next()) {

				int artiste_id = jeuResultat.getInt("artiste_id");
				String artiste_nom = jeuResultat.getString("artiste_nom");
				String artiste_photo = jeuResultat.getString("artiste_photo");
				boolean artiste_membre = jeuResultat.getBoolean("artiste_membre");
				System.out.println("no :" + artiste_id + " nom :" + artiste_nom + " photo : " + artiste_photo + " membre : " + artiste_membre);
				liste.add(new Artiste(artiste_id, artiste_nom, artiste_membre, artiste_photo));
			}
		} catch (SQLException sqle) {
			JOptionPane.showMessageDialog(null, "Probl�me rencontr\u00E8 : " + sqle.getMessage(), "R�sultat",
					JOptionPane.ERROR_MESSAGE);
		}
		return liste;
	}
	private boolean supprimerrEmployeBD() {
		ArrayList<Artiste> liste = new ArrayList<Artiste>();
		Statement statement = null;
		ResultSet jeuResultat = null;
		String requete = "SELECT * FROM Artiste ORDER BY artiste_id";

		try {
			statement = connection.createStatement();
			jeuResultat = statement.executeQuery(requete);
			while (jeuResultat.next()) {

				int artiste_id = jeuResultat.getInt("artiste_id");
				String artiste_nom = jeuResultat.getString("artiste_nom");
				String artiste_photo = jeuResultat.getString("artiste_photo");
				boolean artiste_membre = jeuResultat.getBoolean("artiste_membre");
				System.out.println("no :" + artiste_id + " nom :" + artiste_nom + " photo : " + artiste_photo + " membre : " + artiste_membre);
				liste.add(new Artiste(artiste_id, artiste_nom, artiste_membre, artiste_photo));
			}
		} catch (SQLException sqle) {
			JOptionPane.showMessageDialog(null, "Probl�me rencontr\u00E8 : " + sqle.getMessage(), "R�sultat",
					JOptionPane.ERROR_MESSAGE);
		}
		return liste;
	}

}
