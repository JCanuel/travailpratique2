package modele;

public class Artiste {
	private int numero;
	private String nom;
	private boolean membre;
	private String photo;
	
	public Artiste(int 	artiste_id, String artiste_nom, boolean artiste_membre, String artiste_photo ) {
		numero = 	artiste_id;
		nom = artiste_nom;
		membre = artiste_membre;
		photo = artiste_photo;
	}

	public int getNumero() {
		return numero;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public boolean isMembre() {
		return membre;
	}

	public void setMembre(boolean membre) {
		this.membre = membre;
	}

	public String getPhoto() {
		return photo;
	}

	public void setPhoto(String photo) {
		this.photo = photo;
	}

}
