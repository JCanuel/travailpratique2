package figures;

public class Cercle extends Forme {
	private double rayon;
	
	public Cercle(double x, double y, double r) {
		super(x,y);
		rayon=r;   
	}

	@Override
	public double aire() { 
		return Math.PI*Math.pow(rayon, 2);
	}

	@Override
	public double perimetre(){
		return 2*rayon*Math.PI;
	}

	} 
