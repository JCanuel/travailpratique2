package figures;
abstract public class Forme {
	  protected double posCentreX, posCentreY;
	  //posCentreX, posCentreY sont accessibles par les classes d�riv�es 
	  
	  public Forme(double posCentreX, double posCentreY) {
		this.posCentreX = posCentreX;
		this.posCentreY = posCentreY;
	}
	public void deplacer(double x,double y) {
	    posCentreX =x;
	    posCentreY =y;
	  }
	@Override
	public String  toString() {
	    return "Position ("+posCentreX +", "+ posCentreY+  "); le p�rim�tre est "+
	    perimetre()+" et l'aire est " + aire();
	  }
	  abstract public double aire() ;
	  abstract public double perimetre() ;
	}

