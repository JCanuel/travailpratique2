package figures;

public class Carre extends Forme {
	private double cote;
	
	public Carre(double x, double y, double c) {
		super(x,y);
		cote=c;   
	}

	@Override
	public double aire() { 
		return cote*cote;
		//8
	}

	@Override
	public double perimetre(){
		return cote*4;
	}

	} 